package Modelo;

import android.database.Cursor;

import com.example.exac2_serrano_torres.Ventas;

import java.util.ArrayList;

public interface Proyeccion {
    Ventas getVenta(int numBomba);
    ArrayList<Ventas> allVentas();
    Ventas readVenta(Cursor cursor);
}