package Modelo;

import android.database.Cursor;

import com.example.exac2_serrano_torres.Ventas;

import java.util.ArrayList;

public interface Persistencia {
    void openDataBase();
    void closeDataBase();
    long insertVenta(Ventas venta);

}