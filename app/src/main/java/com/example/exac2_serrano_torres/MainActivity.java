package com.example.exac2_serrano_torres;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import Modelo.VentaDb;

public class MainActivity extends AppCompatActivity {

    private EditText txtNumBomba;
    private RadioGroup rdbTipo;
    private EditText txtPrecio;
    private EditText txtCapacidadBomba;
    private TextView txtContador;
    private EditText txtCantidad;
    private Button btnRegistrarVenta;
    private TextView lblVenta;
    private ArrayList<Ventas> ventasList;
    private MiAdaptador ventaAdapter;
    private VentaDb ventaDb;
    private Ventas venta;

    private BombaGasolina bomba;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        txtNumBomba = findViewById(R.id.txtNumBomba);
        rdbTipo = findViewById(R.id.rdbTipo);
        txtPrecio = findViewById(R.id.txtPrecio);
        txtCapacidadBomba = findViewById(R.id.txtCapacidadBomba);
        txtContador = findViewById(R.id.txtContador);
        txtCantidad = findViewById(R.id.txtCantidad);
        lblVenta = findViewById(R.id.lblVenta);

        ventaDb = new VentaDb(getApplicationContext());
        venta = new Ventas();
        bomba=new BombaGasolina();

        Button btnHacerVenta = findViewById(R.id.btnHacerVenta);
        btnHacerVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hacerVenta();
            }
        });

        btnRegistrarVenta = findViewById(R.id.btnRegistrarVenta);
        btnRegistrarVenta.setEnabled(false);
        btnRegistrarVenta.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                registrarVenta();
            }
        });

        Button btnRegistros = findViewById(R.id.btnRegistros);
        btnRegistros.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mostrarRegistros();
            }
        });

        Button btnLimpiar = findViewById(R.id.btnLimpiar);
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });

        Button btnSalir = findViewById(R.id.btnSalir);
        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });

        Button btnBomba = findViewById(R.id.btnIniciarBomba);
        btnBomba.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bomba();
            }
        });
    }

    public void hacerVenta() {
        String numBomba = txtNumBomba.getText().toString().trim();
        int tipoGasolinaId = rdbTipo.getCheckedRadioButtonId();
        String precioGasolinaStr = txtPrecio.getText().toString().trim();
        String cantidadStr = txtCantidad.getText().toString().trim();

        if (numBomba.isEmpty() || tipoGasolinaId == -1 || precioGasolinaStr.isEmpty() || cantidadStr.isEmpty()) {
            Toast.makeText(this, "Completa todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }

        float precioGasolina;
        int cantidad;

        try {
            precioGasolina = Float.parseFloat(precioGasolinaStr);
            cantidad = Integer.parseInt(cantidadStr);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Ingrese un valor numérico válido para el precio y la cantidad", Toast.LENGTH_SHORT).show();
            return;
        }

        if (Integer.parseInt(txtContador.getText().toString()) == Integer.parseInt(txtCapacidadBomba.getText().toString())) {
            Toast.makeText(this, "Ya se terminó la gasolina", Toast.LENGTH_SHORT).show();
            return;
        }

        if (cantidad > Integer.parseInt(txtCapacidadBomba.getText().toString())) {
            Toast.makeText(this, "No puedes pasar de la gasolina máxima", Toast.LENGTH_SHORT).show();
            return;
        }

        RadioButton selectedRadioButton = findViewById(tipoGasolinaId);
        int tipoGasolina = 0;

        if (selectedRadioButton.getId() == R.id.rdbRegular) {
            tipoGasolina = 1;
        } else if (selectedRadioButton.getId() == R.id.rdbExtra) {
            tipoGasolina = 2;
        }

        Ventas venta = new Ventas(Integer.parseInt(numBomba), tipoGasolina, precioGasolina, cantidad, 0);
        float totalPagar = venta.getPrecioGasolina() * venta.getCantidadGasolina();
        venta.setTotalpa(totalPagar);

        lblVenta.setText("Cantidad: " + venta.getCantidadGasolina() +
                " Precio: " + venta.getPrecioGasolina() +
                " Total a pagar: " + venta.getTotalpa());

        this.venta = venta;
        btnRegistrarVenta.setEnabled(true);

        Toast.makeText(this, "Venta realizada", Toast.LENGTH_SHORT).show();
    }

    private void bomba() {
        String numBomba = txtNumBomba.getText().toString().trim();
        String capacidadBombaStr = txtCapacidadBomba.getText().toString().trim();
        String precioGasolinaStr = txtPrecio.getText().toString().trim();
        int tipoGasolinaId = rdbTipo.getCheckedRadioButtonId();

        if (numBomba.isEmpty() || capacidadBombaStr.isEmpty() || precioGasolinaStr.isEmpty()) {
            Toast.makeText(this, "Completa todos los campos", Toast.LENGTH_SHORT).show();
            return;
        }

        int capacidadBomba;
        float precioGasolina;

        try {
            capacidadBomba = Integer.parseInt(capacidadBombaStr);
            precioGasolina = Float.parseFloat(precioGasolinaStr);
        } catch (NumberFormatException e) {
            Toast.makeText(this, "Ingrese un valor numérico válido para la capacidad de la bomba y el precio de la gasolina", Toast.LENGTH_SHORT).show();
            return;
        }

        RadioButton selectedRadioButton = findViewById(tipoGasolinaId);
        int tipoGasolina = 0;

        if (selectedRadioButton.getId() == R.id.rdbRegular) {
            tipoGasolina = 1;
        } else if (selectedRadioButton.getId() == R.id.rdbExtra) {
            tipoGasolina = 2;
        }

        bomba.setNumBomba(Integer.parseInt(numBomba));
        bomba.setCapacidadBomba(capacidadBomba);
        bomba.setPrecioGasolina(precioGasolina);
        bomba.setTipoGasolina(tipoGasolina);
        bomba.setAcumulador(0);
        txtContador.setText("0");
    }

    private void registrarVenta() {
        long resultado = ventaDb.insertVenta(venta);

        if (resultado != -1) {
            Toast.makeText(this, "Venta guardada correctamente", Toast.LENGTH_SHORT).show();
            Aplicacion app = (Aplicacion) getApplication();
            app.getVentas().add(venta);
            int contadorActual = Integer.parseInt(txtContador.getText().toString());
            int nuevoContador = (int) (contadorActual + venta.getCantidadGasolina());
            txtContador.setText(String.valueOf(nuevoContador));
            btnRegistrarVenta.setEnabled(false);
        } else {
            Toast.makeText(this, "Error al guardar la venta", Toast.LENGTH_SHORT).show();
        }

        Toast.makeText(this, "Ventas registradas", Toast.LENGTH_SHORT).show();
    }

    private void mostrarRegistros() {
        Intent intent = new Intent(this, VentasActivity.class);
        startActivityForResult(intent, 0);
        Toast.makeText(this, "Mostrando registros", Toast.LENGTH_SHORT).show();
    }

    private void limpiar() {
        txtNumBomba.setText("");
        rdbTipo.clearCheck();
        txtPrecio.setText("");
        txtCapacidadBomba.setText("");
        txtContador.setText("");
        txtCantidad.setText("");
        lblVenta.setText("");
    }

    private void salir() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Recibo Gasolina");
        confirmar.setMessage("¿Quieres salir?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });
        confirmar.show();
    }
}